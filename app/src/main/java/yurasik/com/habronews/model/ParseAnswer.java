package yurasik.com.habronews.model;

import java.util.ArrayList;

public class ParseAnswer {
    private boolean result;
    private String error="";
    private ArrayList<ItemModel> items;

    public ParseAnswer(){
        items = new ArrayList<>();
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<ItemModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemModel> items) {
        this.items = items;
    }
}
