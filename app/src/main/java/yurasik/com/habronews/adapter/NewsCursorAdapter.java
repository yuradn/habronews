package yurasik.com.habronews.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.GregorianCalendar;

import yurasik.com.habronews.R;
import yurasik.com.habronews.provider.HabroNewsContentProvider;

public class NewsCursorAdapter extends CursorAdapter{
    private LayoutInflater mInflater; //нужен для создания объектов класса View
    private static String TAG = NewsCursorAdapter.class.getSimpleName();

    public NewsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    public NewsCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View root = mInflater.inflate(R.layout.news_item, parent, false);
        TextView textViewDate  = (TextView) root.findViewById(R.id.textDate);
        TextView textViewTitle = (TextView) root.findViewById(R.id.textTitle);
        ViewHolder holder = new ViewHolder();
        holder.textViewDate     = textViewDate;
        holder.textViewTitle    = textViewTitle;
        root.setTag(holder);
        return root;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(HabroNewsContentProvider.ITEM_ID));
        long longDate     = cursor.getLong(cursor.getColumnIndex(HabroNewsContentProvider.ITEM_DATE));
        String textTitle    = cursor.getString(cursor.getColumnIndex(HabroNewsContentProvider.ITEM_TITLE));
        ViewHolder holder   = (ViewHolder) view.getTag();
        if(holder != null) {
            String myDate = getTextDateTime(longDate);
            holder.textViewDate.setText(myDate);
            //holder.textViewDate.setText(""+longDate);
            holder.textViewTitle.setText(textTitle);
        }
    }


    //Object from best speed
    public static class ViewHolder {
        public TextView textViewDate;
        public TextView textViewTitle;
    }

    public String getTextDateTime(long longDate){

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(longDate);

        int second = gregorianCalendar.get(GregorianCalendar.SECOND);
        String textSecond = ""+second;
        if (second<10) textSecond="0"+textSecond;

        int minute = gregorianCalendar.get(GregorianCalendar.MINUTE);
        String textMinute = ""+minute;
        if (minute<10) textMinute="0"+textMinute;

        int hour = gregorianCalendar.get(GregorianCalendar.HOUR_OF_DAY);
        String textHour = ""+hour;
        if (hour<10) textHour="0"+textHour;

        String textTime = textHour+":"+textMinute+":"+textSecond;

        int day = gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH);
        String textDay = ""+day;
        if (day<10) textDay="0"+textDay;

        int month = gregorianCalendar.get(GregorianCalendar.MONTH);
        String textMonth = ""+month;
        if (month<10) textMonth="0"+textMonth;

        String textYear = Integer.toString(gregorianCalendar.get(GregorianCalendar.YEAR)-2000);

        String textDate = textDay+"/"+textMonth+"/"+textYear;

        return textTime+"\n"+textDate;

    }
}
