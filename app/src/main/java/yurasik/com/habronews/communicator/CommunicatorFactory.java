package yurasik.com.habronews.communicator;

public class CommunicatorFactory {
	public static BackendCommunicator createBackendCommunicator() {
		return new BackendCommunicatorStub();
	}
}
