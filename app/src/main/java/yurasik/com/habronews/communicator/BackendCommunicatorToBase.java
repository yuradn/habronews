package yurasik.com.habronews.communicator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import yurasik.com.habronews.model.ItemModel;
import yurasik.com.habronews.model.ParseAnswer;

/**
 * Created by yura on 29.05.15.
 */
public class BackendCommunicatorToBase implements BackendCommunicator {
        @Override
        public ParseAnswer postParse(String url) throws InterruptedException {
            ParseAnswer parseAnswer = new ParseAnswer();
            try {
                String str="";
                Document doc = Jsoup.connect("http://habrahabr.ru/rss/hubs/").get();
                Elements indexes = doc.select("item");
                for (Element index : indexes){
                    ItemModel item = new ItemModel();

                    Elements titles = index.select("title");
                    Element title = titles.first();
                    str = title.text();
                    String desc = str.replace("<![CDATA[", "");
                    desc = desc.replace("]]>", "");
                    item.setTitle(desc);

                    Elements links = index.select("link");
                    Element link = links.first();
                    //Log.d(TAG,"Link: "+link.nextSibling().toString());
                    item.setLink(link.nextSibling().toString());

                    Elements dates = index.select("pubDate");
                    Element date = dates.first();

                    String ss = date.text();
                    ss=ss.replaceAll("GMT","");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss ", Locale.ENGLISH);
                    Date convertedDate = new Date();
                    try {
                        convertedDate = dateFormat.parse(ss);
                        //Log.d(TAG, "***********"+convertedDate);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    GregorianCalendar calendar = new GregorianCalendar();
                    calendar.setTime(convertedDate);
                    item.setPubDate(calendar.getTimeInMillis());

//				Elements categories = index.select("category");
//				for (Element category : categories) {
//					item.getCategories().add(category.toString());
//				}

//				Elements authors = index.select("author");
//				Element author = authors.first();
//				item.setAuthor(author.toString());

                    parseAnswer.getItems().add(0,item);
                    //parseAnswer.getItems().add(item);
                }

                parseAnswer.setResult(true);
                //Thread.sleep(2000);
            } catch (IOException e) {
                e.printStackTrace();
                parseAnswer.setResult(false);
                parseAnswer.setError(e.toString());
                return parseAnswer;
            }
            return parseAnswer;
        }
    }
