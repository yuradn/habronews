package yurasik.com.habronews.communicator;


import yurasik.com.habronews.model.ParseAnswer;

public interface BackendCommunicator {
	ParseAnswer postParse(String url) throws InterruptedException;
}
