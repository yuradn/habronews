package yurasik.com.habronews.communicator;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import yurasik.com.habronews.MainActivity;
import yurasik.com.habronews.model.ItemModel;
import yurasik.com.habronews.model.ParseAnswer;


class BackendCommunicatorStub implements BackendCommunicator {
	private final static String TAG = "BackendCommunicator";


	@Override
	public ParseAnswer postParse(String url) throws InterruptedException {
		ParseAnswer parseAnswer = new ParseAnswer();
		try {
			String str="";
			Document doc = Jsoup.connect(MainActivity.PARSE_SITE_ADDRESS).get();
			Elements indexes = doc.select("item");
			for (Element index : indexes){
				ItemModel item = new ItemModel();

				Elements titles = index.select("title");
				Element title = titles.first();
				str = title.text();
				String desc = str.replace("<![CDATA[", "");
				desc = desc.replace("]]>", "");
				item.setTitle(desc);

				Elements links = index.select("link");
				Element link = links.first();
				//Log.d(TAG,"Link: "+link.nextSibling().toString());
				item.setLink(link.nextSibling().toString());

				Elements dates = index.select("pubDate");
				Element date = dates.first();

				String ss = date.text();
				ss=ss.replaceAll("GMT","");
				SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss ", Locale.ENGLISH);
				Date convertedDate = new Date();
				try {
					convertedDate = dateFormat.parse(ss);
					//Log.d(TAG, "***********"+convertedDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(convertedDate);
				item.setPubDate(calendar.getTimeInMillis());

//				Elements categories = index.select("category");
//				for (Element category : categories) {
//					item.getCategories().add(category.toString());
//				}

//				Elements authors = index.select("author");
//				Element author = authors.first();
//				item.setAuthor(author.toString());

				parseAnswer.getItems().add(0,item);
				//parseAnswer.getItems().add(item);
			}

			parseAnswer.setResult(true);
			//Thread.sleep(2000);
		} catch (IOException e) {
			e.printStackTrace();
			parseAnswer.setResult(false);
			parseAnswer.setError(e.toString());
			return parseAnswer;
		}


		parseHabr(MainActivity.PARSE_SITE_ADDRESS);

		return parseAnswer;
	}

	private ParseAnswer parseHabr(String url) {
		Log.d(TAG, "Parse");

		try {
			URL ur = new URL(url);
			HttpURLConnection con = (HttpURLConnection) ur.openConnection();
			//readStream(con.getInputStream());

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();


			org.w3c.dom.Document doc = db.parse(con.getInputStream());
			NodeList entries = doc.getElementsByTagName("item");
			int num = entries.getLength();
			Log.d(TAG,"Number of titles: "+num);
			for (int i=0; i<num; i++) {
				org.w3c.dom.Element node = (org.w3c.dom.Element) entries.item(i);
				listAllAttributes(node);
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void listAllAttributes(org.w3c.dom.Element element) {

		Log.d(TAG, "List attributes for node: " + element.getNodeName());

		// get a map containing the attributes of this node
		NamedNodeMap attributes = element.getAttributes();

		// get the number of nodes in this map
		int numAttrs = attributes.getLength();

		for (int i = 0; i < numAttrs; i++) {
			Attr attr = (Attr) attributes.item(i);

			String attrName = attr.getNodeName();
			String attrValue = attr.getNodeValue();

			Log.d(TAG,"Found attribute: " + attrName + " with value: " + attrValue);

		}
	}

	private void readStream(InputStream in) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
