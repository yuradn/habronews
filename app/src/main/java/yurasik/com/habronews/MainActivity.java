package yurasik.com.habronews;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import yurasik.com.habronews.fragment.ListFragment;
import yurasik.com.habronews.fragment.MenuFragment;


public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();

    public static final String FRAGMENT_MENU = "activity.fragment";
    public static final String FRAGMENT_BACKGROUND = "background.fragment";
    public static final String FRAGMENT_LIST = "list.fragment";
    public static final String FRAGMENT_DB = "db.fragment";
    public static final String PARSE_SITE_ADDRESS = "http://habrahabr.ru/rss/hubs/";
    public static final String WEB_ADDRESS = "web.address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_MENU);
        if (menuFragment == null) {
            menuFragment = new MenuFragment();
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentMenu, menuFragment, FRAGMENT_MENU);
            manager.commit();
        }

        ListFragment listFragment = (ListFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST);
        if (listFragment == null) {
            listFragment = new ListFragment();
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentList, listFragment, FRAGMENT_LIST);
            manager.commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}