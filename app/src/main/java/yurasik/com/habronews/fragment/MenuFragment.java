package yurasik.com.habronews.fragment;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alertdialogpro.AlertDialogPro;
import com.gc.materialdesign.views.Button;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import yurasik.com.habronews.MainActivity;
import yurasik.com.habronews.R;
import yurasik.com.habronews.model.ItemModel;
import yurasik.com.habronews.parse.ParseBackroundFragment;
import yurasik.com.habronews.parse.ParseModel;
import yurasik.com.habronews.provider.HabroNewsContentProvider;

public class MenuFragment extends Fragment implements ParseModel.Observer{
    private final static String TAG = MenuFragment.class.getSimpleName();

    private View mProgress;
    private Button startParse;
    private ParseModel mParseModel;
    private AlertDialog alert;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main,null,false);

        mProgress = v.findViewById(R.id.view_progress);

        startParse = (Button) v.findViewById(R.id.buttonStartParse);
        startParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParseModel.signIn(getResources().getString(R.string.http_address));
            }
        });

        ParseBackroundFragment parseBackroundFragment;

        parseBackroundFragment =
                (ParseBackroundFragment) getFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_BACKGROUND);

        if (parseBackroundFragment != null) {
            mParseModel = parseBackroundFragment.getParseModel();
        } else {
            parseBackroundFragment = new ParseBackroundFragment();

            getFragmentManager().beginTransaction()
                    .add(parseBackroundFragment, MainActivity.FRAGMENT_BACKGROUND)
                    .commit();

            mParseModel = parseBackroundFragment.getParseModel();
        }

        mParseModel.registerObserver(this);

        if (mParseModel.getParseAnswer()!=null)
            if (!mParseModel.getParseAnswer().isResult()) alertDialog(mParseModel);

        return v;
    }




    private void showProgress(final boolean show) {
        startParse.setEnabled(!show);
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void alertDialog(final ParseModel parseModel){
        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
        builder.setTitle("Error!")
                .setMessage(parseModel.getParseAnswer().getError())
                .setIcon(R.drawable.abc_list_selector_disabled_holo_dark)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                parseModel.getParseAnswer().setResult(true);
                                dialog.cancel();
                            }
                        });

        alert = builder.create();
        alert.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mParseModel.unregisterObserver(this);

        if (getActivity().isFinishing()) {
            mParseModel.stopSignIn();
        }

        if (alert!=null)
            if (alert.isShowing())
                alert.dismiss();

    }

    @Override
    public void onParseStarted(ParseModel parseModel) {
        Log.i(TAG, "onParseStarted");
        showProgress(true);
    }

    @Override
    public void onParseSucceeded(ParseModel parseModel) {
        Log.i(TAG, "onParseSucceeded");
        showProgress(false);
//        for (ItemModel item : parseModel.getParseAnswer().getItems()) {
//            Log.d(TAG,"Item: "+item);
//        }

        AddToDB addToDB = new AddToDB();
        addToDB.execute(parseModel.getParseAnswer().getItems());
        //getActivity().getSupportLoaderManager().getLoader(ListFragment.LOADER_ID).forceLoad();
    }

    @Override
    public void onParseFailed(ParseModel parseModel) {
        Log.i(TAG, "onParseFailed");
        showProgress(false);
        alertDialog(parseModel);
    }

    public class AddToDB extends AsyncTask<ArrayList<ItemModel>,Void, Void> {
        @Override
        protected Void doInBackground(ArrayList<ItemModel>... params) {
            ArrayList<ItemModel> items = params[0];
            ContentValues[] cvItems = new ContentValues[items.size()];

            for (int i=0;i<items.size();i++) {
                ContentValues cv = new ContentValues();
                cv.put(HabroNewsContentProvider.ITEM_TITLE, items.get(i).getTitle());
                cv.put(HabroNewsContentProvider.ITEM_LINK, items.get(i).getLink());
                cv.put(HabroNewsContentProvider.ITEM_DATE, items.get(i).getPubDate());
                cvItems[i] = cv;
            }

            getActivity().getContentResolver().bulkInsert(HabroNewsContentProvider.ITEM_CONTENT_URI, cvItems);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                getActivity().getSupportLoaderManager().getLoader(ListFragment.LOADER_ID).forceLoad();
            } catch (Exception e) {
                Log.e(TAG,e.toString());
            }
        }
    }
}
