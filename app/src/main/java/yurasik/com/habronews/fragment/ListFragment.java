package yurasik.com.habronews.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import yurasik.com.habronews.MainActivity;
import yurasik.com.habronews.R;
import yurasik.com.habronews.WebViewActivity;
import yurasik.com.habronews.adapter.NewsCursorAdapter;
import yurasik.com.habronews.model.ItemModel;
import yurasik.com.habronews.provider.CustomCursorLoader;
import yurasik.com.habronews.provider.HabroNewsContentProvider;

public class ListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {

    private final static String TAG = ListFragment.class.getSimpleName();
    private ListView lvNews;
    private NewsCursorAdapter newsCursorAdapter;

    final Uri ITEM_URI = Uri
            .parse("content://yurasik.com.habronews.provider/items");
    public final static int LOADER_ID = 12345;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, null, false);

        newsCursorAdapter = new NewsCursorAdapter(getActivity(), null, 0);
        getActivity().getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        lvNews = (ListView) v.findViewById(R.id.listNews);
        lvNews.setAdapter(newsCursorAdapter);

        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "Item on click position: " + position + " id: " + id);
                Uri uri = Uri.parse(HabroNewsContentProvider.ITEM_CONTENT_URI + "/" + id);
                Log.d(TAG, uri.toString());
                try {
                    Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                    cursor.moveToFirst();
                    String address = cursor.getString(2);

                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra(MainActivity.WEB_ADDRESS, address);

                    cursor.close();

                    startActivity(intent);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });


        return v;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        CustomCursorLoader customCursorLoader = new CustomCursorLoader(getActivity());
        Log.d(TAG, "onCreateLoader: " + customCursorLoader);
        return customCursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.d(TAG, "onLoadFinished.");
        newsCursorAdapter.swapCursor(cursor);
        //newsCursorAdapter.notifyDataSetChanged();
        // scAdapter.swapCursor(cursor);
        // scAdapter.notifyDataSetChanged();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
