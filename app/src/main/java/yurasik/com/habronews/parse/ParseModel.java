package yurasik.com.habronews.parse;

import android.database.Observable;
import android.os.AsyncTask;
import android.util.Log;

import yurasik.com.habronews.communicator.BackendCommunicator;
import yurasik.com.habronews.communicator.CommunicatorFactory;
import yurasik.com.habronews.model.ParseAnswer;

public class ParseModel {

    private static final String TAG = "ParseModel";

    private final ParseObservable mObservable = new ParseObservable();
    private ParseTask mParseTask;
    private boolean mIsWorking = false;

    private ParseAnswer parseAnswer;


    ParseModel(){
        Log.i(TAG, "************ new Instance");
    }

    public void signIn(final String url) {
        if (mIsWorking) {
            return;
        }

        mObservable.notifyStarted();

        mIsWorking = true;
        mParseTask = new ParseTask(url);
        mParseTask.execute();
    }

    public void stopSignIn() {
        if (mIsWorking) {
            mParseTask.cancel(true);
            mIsWorking = false;
        }
    }

    public void registerObserver(final Observer observer) {
        mObservable.registerObserver(observer);
        if (mIsWorking) {
            observer.onParseStarted(this);
        }
    }

    public void unregisterObserver(final Observer observer) {
        mObservable.unregisterObserver(observer);
    }

    private class ParseTask extends AsyncTask<Void, Void, ParseAnswer> {
        private String url;

        public ParseTask(final String url) {
            this.url = url;
        }

        @Override
        protected ParseAnswer doInBackground(final Void... params) {
            final BackendCommunicator communicator = CommunicatorFactory.createBackendCommunicator();

            try {
                return  communicator.postParse(url);
            } catch (InterruptedException e) {
                Log.i(TAG, "Parse interrupted");
                ParseAnswer parseAnswer = new ParseAnswer();
                parseAnswer.setError(e.toString());
                parseAnswer.setResult(false);
                return parseAnswer;
            }
        }

        @Override
        protected void onPostExecute(final ParseAnswer success) {
            mIsWorking = false;

            if (success.isResult()) {
                mObservable.notifySucceeded(success);
            } else {
                mObservable.notifyFailed(success);
            }
        }
    }


    public interface Observer {
        void onParseStarted(ParseModel parseModel);

        void onParseSucceeded(ParseModel parseModel);

        void onParseFailed(ParseModel parseModel);
    }

    private class ParseObservable extends Observable<Observer> {
        public void notifyStarted() {
            for (final Observer observer : mObservers) {
                observer.onParseStarted(ParseModel.this);
            }
        }

        public void notifySucceeded(ParseAnswer success) {
            for (final Observer observer : mObservers) {
                setParseAnswer(success);
                observer.onParseSucceeded(ParseModel.this);
            }
        }

        public void notifyFailed(ParseAnswer success) {
            for (final Observer observer : mObservers) {
                setParseAnswer(success);
                observer.onParseFailed(ParseModel.this);
            }
        }
    }

    public ParseAnswer getParseAnswer() {
        return parseAnswer;
    }

    public void setParseAnswer(ParseAnswer parseAnswer) {
        this.parseAnswer = parseAnswer;
    }
}
