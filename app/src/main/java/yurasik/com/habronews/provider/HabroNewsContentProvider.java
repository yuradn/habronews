package yurasik.com.habronews.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class HabroNewsContentProvider extends ContentProvider{
    private final String TAG = HabroNewsContentProvider.class.getSimpleName();

    // // Константы для БД
    // БД
    public static final String DB_NAME = "habr_rss";
    public static final int DB_VERSION = 1;

    // Таблица
    public static final String ITEM_TABLE = "items";

    // Поля
    public static final String ITEM_ID      = "_id";
    public static final String ITEM_TITLE   = "title";
    public static final String ITEM_LINK    = "link";
    public static final String ITEM_DATE    = "date";

    // Скрипт создания таблицы
    public static final String DB_CREATE = "CREATE TABLE " + ITEM_TABLE + " ("
            + ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ITEM_TITLE + " TEXT, " + ITEM_LINK+" TEXT, " + ITEM_DATE
            + " INTEGER NOT NULL, UNIQUE( " + ITEM_DATE + " ) ON CONFLICT IGNORE" + ");";

    // // Uri
    // authority
    static final String AUTHORITY = "yurasik.com.habronews.provider";
    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    // path
    static final String ITEM_PATH = "items";

    // Общий Uri
    public static final Uri ITEM_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + ITEM_PATH);

    // Типы данных
    // набор строк
    static final String ITEM_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + ITEM_PATH;

    // одна строка
    static final String ITEM_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + ITEM_PATH;

    //// UriMatcher
    // общий Uri
    static final int URI_ITEMS = 1;

    // Uri с указанным ID
    static final int URI_ITEMS_ID = 2;

    // описание и создание UriMatcher
    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, ITEM_PATH, URI_ITEMS);
        uriMatcher.addURI(AUTHORITY, ITEM_PATH + "/#", URI_ITEMS_ID);
    }

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext(), ITEM_TABLE, null, DB_VERSION);
        return false;
    }

    // чтение
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query, " + uri.toString());
        // проверяем Uri
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS: // общий Uri
                Log.d(TAG, "URI_ITEMS");
                // если сортировка не указана, ставим свою - по имени
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ITEM_DATE + " DESC";
                }
                break;
            case URI_ITEMS_ID: // Uri с ID
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_ITEMS_ID, " + id);
                // добавляем ID к условию выборки
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(ITEM_TABLE, projection, selection,
                selectionArgs, null, null, sortOrder);
        // просим ContentResolver уведомлять этот курсор
        // об изменениях данных в CONTACT_CONTENT_URI
        cursor.setNotificationUri(getContext().getContentResolver(),
                ITEM_CONTENT_URI);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        Log.d(TAG, "getType, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                return ITEM_CONTENT_TYPE;
            case URI_ITEMS_ID:
                return ITEM_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(TAG, "insert, " + uri.toString());
        if (uriMatcher.match(uri) != URI_ITEMS)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        db = dbHelper.getWritableDatabase();
        long rowID = db.insert(ITEM_TABLE, null, values);
        Uri resultUri = ContentUris.withAppendedId(ITEM_CONTENT_URI, rowID);
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        Log.d(TAG, "bulk insert values " + uri.toString());
        if (uriMatcher.match(uri) != URI_ITEMS)
            throw new IllegalArgumentException("Wrong URI: " + uri);
        db = dbHelper.getWritableDatabase();

        for (int i=0; i<values.length; i++) {
            long rowID = db.insert(ITEM_TABLE, null, values[i]);
            Uri resultUri = ContentUris.withAppendedId(ITEM_CONTENT_URI, rowID);
            // уведомляем ContentResolver, что данные по адресу resultUri изменились
            getContext().getContentResolver().notifyChange(resultUri, null);
        }

        return super.bulkInsert(uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG, "delete, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                Log.d(TAG, "URI_ITEMS");
                break;
            case URI_ITEMS_ID:
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_ITEMS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.delete(ITEM_TABLE, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(TAG, "update, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                Log.d(TAG, "URI_ITEMS");

                break;
            case URI_ITEMS_ID:
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_ITEMS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.update(ITEM_TABLE, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }




}

class DBHelper extends SQLiteOpenHelper {
    private final static String TAG=DBHelper.class.getSimpleName();

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HabroNewsContentProvider.DB_CREATE);
//        ContentValues cv = new ContentValues();
//        for (int i = 1; i <= 10; i++) {
//            try {
//                TimeUnit.MILLISECONDS.sleep(10L);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            cv.put(HabroNewsContentProvider.ITEM_TITLE, "title " + i);
//            cv.put(HabroNewsContentProvider.ITEM_LINK,"http://www.visitka.dp.ua/");
//            GregorianCalendar calendar = new GregorianCalendar();
//            calendar.getTimeInMillis();
//            cv.put(HabroNewsContentProvider.ITEM_DATE, calendar.getTimeInMillis());
//            db.insert(HabroNewsContentProvider.ITEM_TABLE, null, cv);
//        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}