package yurasik.com.habronews.provider;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.util.Log;

public class CustomCursorLoader extends CursorLoader {
    private final static String TAG = CustomCursorLoader.class.getSimpleName();

    //private final ForceLoadContentObserver mObserver = new ForceLoadContentObserver();

    public CustomCursorLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    public CustomCursorLoader(Context context) {
        super(context);
    }



    @Override
    public Cursor loadInBackground() {
        Cursor cursor = getContext().getContentResolver().query(HabroNewsContentProvider.ITEM_CONTENT_URI, null, null,
                null, null); // get your cursor from wherever you like
        Log.d(TAG, "LoadInBackgroind: cursor= " + cursor.toString());

        return cursor;
    }
};